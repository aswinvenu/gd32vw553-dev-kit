GD32VW553 Development Kit

https://www.gigadevice.com/product/mcu/risc-v

Based on GD32VW553HMQ6

Series    : GD32VW553
Package   : QFN40
Max Speed : 160MHz
Flash     : 4096K
SRAM	  : 320K
IO upto   : 28

GPTM(32 bit)  : 2
GPTM(16 bit)  : 2

I2C	      : 2
SPI	      : 1
USART+UART    : 1+2
QSPI	      : 1


Lacking : 

USB
